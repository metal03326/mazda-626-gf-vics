# Mazda 626 GF VICS

Arduino Pro Mini based controller for the Variable Inertia Charging System inside the intake manifold of the Mazda FS engine.

## The car

This project has been developed for 2001 Mazda 626 with 1.8 FP engine, but it should work for other car models that have a separate wire for engine RPM (non-CAN cars).

#### Locating RPM wire on that car

Cable that carries the RPM signal on the EDM 626 GF 1999+ (I do not know about pre-facelifts) is violet with white (V/W). It connects terminal 3B of the Instrument Cluster's 20 pin connector and terminal 48 of the PCM. On my car the violet color is vivid on the IC side, but very pale (looks like pink) on the PCM side.

## The Arduino

The project makes use of **Arduino Pro Mini**, but you could use any other Arduino that has at least one **External Interrupt** and one **Digital I/O** pins. 

## The code

Source code is written with [Arduino](https://www.arduino.cc/) 1.8.9.

According to the documentation I have found on the internet about the VICS operation for Mazda Protege (USDM Mazda 323), the vacuum actuator should be activated 5 seconds after engine has been started. This has been implemented in current version of the code.

RPM level at which the vacuum actuator is de-activated should very between engine models, but for the current version of the code it is 4250. I never did proper tuning with this for the FP engine, dough.

In order to minimize changes of oscillation, the vacuum actuator will not be re-activated once RPM fall below 4250, but instead 4000.

All numbers are configurable. 

## Schematics

![](schematics/VICS.png)

| Item             | Type              | Qty  |
| :---             | :---              | ---: |
| Arduino Pro Mini | Microcontroller   | 1    |
| AUIRL1404ZL      | NMOS Transistor   | 1    |
| FR104            | Diode             | 1    |
| P6KE33A          | Transil Diode     | 1    |
| 1N5819           | Schottky Diode    | 1    |
| 33Ω              | Resistor          | 1    |
| 10kΩ             | Resistor          | 3    |
| 330uF            | Ceramic capacitor | 1    |
| 100nF            | Ceramic capacitor | 1    |

Schematics have been done with [Circuit Diagram](https://www.circuit-diagram.org/). Check [schematics](schematics) folder.

All components can be fitted to a hat that fits the Arduino Pro Mini.

## Video of installing VICS on FP motor

[![](http://img.youtube.com/vi/u-u-nPaqKtM/0.jpg)](http://www.youtube.com/watch?v=u-u-nPaqKtM "")

https://www.youtube.com/embed/u-u-nPaqKtM

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
