/*
  Mazda 626 GF VICS v1.0.0

  Arduino Pro Mini based controller for the Variable Inertia Charging System
  inside the intake manifold of the Mazda FS (rarely FP) engine.

  Build-in LED is being turned on and off with the actuator - if LED is lid
  then actuator is activated. This is done to ease computer-less inspection
  from within the car.

  Serial is also left running to easy debugging with computer.

  This code along with the schematics are available at
  https://gitlab.com/metal03326/mazda-626-gf-vics

  Code for reading the RPM was extracted from
  https://github.com/deepsyx/arduino-tachometer
*/

const int NUMBER_OF_CYLINDERS = 4;
const int LED_UPDATE_INTERVAL = 200;

// Operate VICS 5 seconds after engine starts
const int CLOSE_AFTER_N_ITERATIONS = 5000 / LED_UPDATE_INTERVAL;
unsigned int INITIAL_CLOSE_TIMER = 0;

// Set RPM at which VICS will operate. CLOSE should be lower than OPEN
const int OPEN_THRESHOLD = 4250;
const int CLOSE_THRESHOLD = 4000;

// Set input and output pins
const int RPM_INPUT_PIN = 3;
const int ACTUATOR_OUTPUT_PIN = 11;

// 4 stroke engine fires every spark in 2 revolutions
// so calculate at what degree interval sparks fires and divide 360 by it,
// to find the number of fires per rotation
const int FIRES_PER_REV = (360 / (720 / NUMBER_OF_CYLINDERS));

/*
 * Last led state update time in ms, used to calculate the time from last update
 */
unsigned long lastUpdateTime  = 0;

/*
 * Amount of spark fires in a single interval
 */
volatile int sparkFireCount            = 0;

/*
 * Rpm value from last update
 * Used to average the last 2 rpms for smoother output
 */
int lastRpmValue              = 0;

/*
 *
 */
void incrementRpmCount () {
  sparkFireCount++;
}


void setup() {
  // Start listening for RPM input
  attachInterrupt(digitalPinToInterrupt(RPM_INPUT_PIN), incrementRpmCount, FALLING);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(ACTUATOR_OUTPUT_PIN, OUTPUT);

  // Make sure both vacuum actuator and build in LED are not on
  digitalWrite(ACTUATOR_OUTPUT_PIN, LOW);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(9600);
}

void loop() {
  if ((millis() - lastUpdateTime) > LED_UPDATE_INTERVAL) {

    // multiply the amount the spark fires in one interval by the number of intervals per
    // second, to find the amount in one second
    // then multiply the amount in one second by 60, to find the spark fires in one minute and
    // divide the result by the number of fires per revolution to find the rpm
    int currentRpm = (sparkFireCount * (1000 / LED_UPDATE_INTERVAL) * 60) / FIRES_PER_REV;

    // average the current and last rpm for smoother results
    int averagedRpm = (currentRpm + lastRpmValue) / 2;

    Serial.println(averagedRpm);

    // Wait for the engine to start
    if (averagedRpm > 500) {
      // Do not do anything to do with RPM until initial 5s elapse
      if (INITIAL_CLOSE_TIMER < CLOSE_AFTER_N_ITERATIONS) {
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(ACTUATOR_OUTPUT_PIN, LOW); 
    
        INITIAL_CLOSE_TIMER++;
      } else if (averagedRpm >= OPEN_THRESHOLD) {
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(ACTUATOR_OUTPUT_PIN, LOW);
      } else if (averagedRpm < CLOSE_THRESHOLD) {
        digitalWrite(LED_BUILTIN, HIGH);
        digitalWrite(ACTUATOR_OUTPUT_PIN, HIGH);
      }
    } else {
      // Handle engine stall
      INITIAL_CLOSE_TIMER = 0;
      
      digitalWrite(LED_BUILTIN, LOW);
      digitalWrite(ACTUATOR_OUTPUT_PIN, LOW);
    }

    sparkFireCount = 0;
    lastUpdateTime = millis();
    lastRpmValue = currentRpm;
  }
}
